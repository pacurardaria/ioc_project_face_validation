// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include "Functions.h"
#include <queue>
#include <stdio.h>

//-------------Pentru fisiere: scriere, citire----------------
#include <iostream>
#include <fstream>
#include <conio.h>
//------------------------------------------------------------
using namespace std;

CascadeClassifier face_cascade;

int globalHistoH[256] = { 0 };
int globalHistoS[256] = { 0 };

float stdDevH = 7166.37;
float stdDevS = 4768.24;

float mediaH = 17.56;
float mediaS = 83.82;

int globalHistoHF[256] = { 0 };
int globalHistoSF[256] = { 0 };




bool isInside(int i, int j, Mat src) {

	if ((i >= 0 && i < src.cols) && (j >= 0 && j < src.rows)) {
		return true;
	}
	return false;
}

void saveGlobalHistoH(){
	FILE *fp;

	fp = fopen("HistogramaGlobalaH.txt", "w+");

	for (int count = 0; count < 256; count++){
		fprintf(fp, "%d\n", globalHistoHF[count]);
	}
	fprintf(fp, "%.2f\n", mediaH);
	fprintf(fp, "%.2f\n", stdDevH);
	fclose(fp);
}

void saveGlobalHistoS(){
	FILE *fp;

	fp = fopen("HistogramaGlobalaS.txt", "w+");

	for (int count = 0; count < 256; count++){
		fprintf(fp, "%d\n", globalHistoSF[count]);

	}
	fprintf(fp, "%.2f\n", mediaS);
	fprintf(fp, "%.2f\n", stdDevS);
	fclose(fp);

}

void readGlobalHistoH(){
	FILE *fp;
	int i = 0;
	int nr = -1;
	fp = fopen("HistogramaGlobalaH.txt", "r");
	for (int i = 0; i < 256; i++) {
		fscanf(fp, "%d\n", &nr);
		globalHistoH[i++] = nr;
	}
	fscanf(fp, "%.2f\n", &mediaH);
	fscanf(fp, "%.2f\n", &stdDevH);
	printf("%f\n %f\n", mediaH, stdDevH);

	fclose(fp);
}

void readGlobalHistoS(){
	FILE *fp;
	int i = 0;
	int nr = -1;
	fp = fopen("HistogramaGlobalaS.txt", "r");
	for (int i = 0; i < 256; i++) {
		fscanf(fp, "%d\n", &nr);
		globalHistoS[i++] = nr;
	}
	fscanf(fp, "%.2f\n", &mediaS);
	fscanf(fp, "%.2f\n", &stdDevS);
	printf("%f\n %f\n", mediaS, stdDevS);
	fclose(fp);
}

void readParameters(){
	printf("BEFORE");
	for (int i = 0; i < 256; i++) {
		printf("H: %d \n S: %d\n", globalHistoH[i], globalHistoS[i]);
	}
	printf("Media: %.2f Deviatia: %.2f\n", mediaH, stdDevH);

	readGlobalHistoH();
	readGlobalHistoS();
	
	printf("AFTER");
	for (int i = 0; i < 256; i++) {
		printf("H: %d \n S: %d\n", globalHistoH[i], globalHistoS[i]);
	}
	printf("Media: %.2f Deviatia: %.2f", mediaH, stdDevH);

}

void ftjGaussian() {
	float gauss[7];
	float sqrt2pi = sqrtf(2 * PI);
	float sigma = 1.5;
	float e = 2.718;
	float sum = 0;

	// Construire gaussian
	for (int i = 0; i<7; i++) {
		gauss[i] = 1.0 / (sqrt2pi*sigma)* powf(e, -(float)(i - 3)*(i - 3)
			/ (2 * sigma*sigma));
		sum += gauss[i];
	}
	// Filtrare cu gaussian
	for (int j = 3; j < 256 - 3; j++)
	{
		for (int i = 0; i < 7; i++) {
			globalHistoHF[j] += (float)globalHistoH[j + i - 3] * gauss[i];
			globalHistoSF[j] += (float)globalHistoS[j + i - 3] * gauss[i];
		}
	}

	int ThH = 350, ThS=250;
	for (int i = 0; i < 256; i++) {
		if (globalHistoHF[i] < ThH) {
			globalHistoHF[i] = 0;
		}
		if (globalHistoSF[i] < ThS) {
			globalHistoSF[i] = 0;
		}
	}
}

/*
*	Segmentare pe H 
*/
boolean segmentareH(int x, int y, void *userdata) {
	Mat *H = (Mat*)userdata;

	int width = (*H).cols;
	int height = (*H).rows;
	Mat labels = Mat::zeros((*H).size(), CV_16UC1);
	Mat dst = Mat::zeros((*H).size(), CV_8UC1);
	queue <Point> que;
	double hue_avg = mediaH;

	int k = 1;
	int N = 1;
	float T = 15;
	que.push(Point(x, y));

	int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
	int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

	while (!que.empty()) {
		Point oldest = que.front();
		que.pop();

		int xx = oldest.x;
		int yy = oldest.y;

		for (int i = 0; i < 8; i++) {
			if (isInside(xx + nX[i], yy + nY[i], *H)) {
				if ((abs((*H).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avg) < k * stdDevH)
					&& labels.at<ushort>(yy + nY[i], xx + nX[i]) == 0) {

					que.push(Point(xx + nX[i], yy + nY[i]));
					labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
					hue_avg = (N * hue_avg + (*H).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
					N++;
				}
			}
		}
	}

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (labels.at<ushort>(i, j) == 1) {
				dst.at<uchar>(i, j) = 255;
			}
		}
	}

	Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
	erode(dst, dst, element1, Point(-1, -1), 2);
	dilate(dst, dst, element1, Point(-1, -1), 4);
	erode(dst, dst, element1, Point(-1, -1), 2);

	if (N / (width * height) > 0.6) {
		return true;
	}
	return false;
}

/*
* Segmentare pe S
*/
boolean segmentareS(int x, int y, void *userdata) {
	Mat *S = (Mat*)userdata;

	int width = (*S).cols;
	int height = (*S).rows;
	Mat labels = Mat::zeros((*S).size(), CV_16UC1);
	Mat dst = Mat::zeros((*S).size(), CV_8UC1);
	queue <Point> que;
	double hue_avg = mediaS;

	int k = 1;
	int N = 1;
	float T = 15;
	que.push(Point(x, y));

	int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
	int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

	while (!que.empty()) {
		Point oldest = que.front();
		que.pop();

		int xx = oldest.x;
		int yy = oldest.y;

		for (int i = 0; i < 8; i++) {
			if (isInside(xx + nX[i], yy + nY[i], *S)) {
				if ((abs((*S).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avg) < k * stdDevS)
					&& labels.at<ushort>(yy + nY[i], xx + nX[i]) == 0) {

					que.push(Point(xx + nX[i], yy + nY[i]));
					labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
					hue_avg = (N * hue_avg + (*S).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
					N++;
				}
			}
		}
	}

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (labels.at<ushort>(i, j) == 1) {
				dst.at<uchar>(i, j) = 255;
			}
		}
	}

	Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
	erode(dst, dst, element1, Point(-1, -1), 2);
	dilate(dst, dst, element1, Point(-1, -1), 4);
	erode(dst, dst, element1, Point(-1, -1), 2);

	if (N / (width * height) > 0.6) {
		return true;
	}
	return false;
}

/*
* Segmentare H + S
*/
boolean segmentareHS(int x, int y, void *userdata1, void *userdata2) {
	Mat *S = (Mat*)userdata1;
	Mat *H = (Mat*)userdata2;

	int widthS = (*S).cols;
	int heightS = (*S).rows;

	int widthH = (*H).cols;
	int heightH = (*H).rows;

	Mat labels = Mat::zeros((*S).size(), CV_16UC1);
	Mat dst = Mat::zeros((*S).size(), CV_8UC1);
	queue <Point> que;
	double hue_avgS = mediaS;
	double hue_avgH = mediaH;

	int k = 1;
	int N = 1;
	float T = 15;
	que.push(Point(x, y));

	int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
	int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

	while (!que.empty()) {
		Point oldest = que.front();
		que.pop();

		int xx = oldest.x;
		int yy = oldest.y;

		for (int i = 0; i < 8; i++) {
			if (isInside(xx + nX[i], yy + nY[i], *S)) {
				double d = sqrt(pow(abs((*H).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avgH), 2) + pow(abs((*S).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avgS), 2));
				if (d < T) {
					que.push(Point(xx + nX[i], yy + nY[i]));
					labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
					hue_avgH = (N * hue_avgH + (*H).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
					hue_avgS = (N * hue_avgS + (*S).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);

					N++;
				}
			}
		}
	}

	for (int i = 0; i < heightH; i++) {
		for (int j = 0; j < widthH; j++) {
			if (labels.at<ushort>(i, j) == 1) {
				dst.at<uchar>(i, j) = 255;
			}
		}
	}

	Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
	erode(dst, dst, element1, Point(-1, -1), 2);
	dilate(dst, dst, element1, Point(-1, -1), 4);
	erode(dst, dst, element1, Point(-1, -1), 2);

	if (N / (widthH * heightH) > 0.6) {
		return true;
	}
	return false;

}

/*
	Validare fete pe imagini statice
*/
void faceDetectDisplay(const string& window_name, Mat frame, int minFaceSize, int method) {
	std::vector<Rect> faces;
	Mat frame_gray;
	Mat hsv;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	cvtColor(frame, hsv, CV_BGR2HSV);
	equalizeHist(frame_gray, frame_gray);

	vector<Mat> channels;
	split(hsv, channels);

	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(minFaceSize, minFaceSize));

	for (int i = 0; i < faces.size(); i++)
	{
		faces[i].x = faces[i].x + faces[i].width / 9;
		faces[i].y = faces[i].y + faces[i].width / 9;
		faces[i].width = faces[i].width * 7 / 9;
		faces[i].height = faces[i].height * 7 / 9;

		Mat faceROIH = channels[0](faces[i]) * 255 / 180;
		Mat faceROIS = channels[1](faces[i]);
		
		switch (method) {
		case 0:
			if (segmentareH(0, 0, &faceROIH)) {
				rectangle(frame, faces[i], Scalar(0, 255, 0));
			}
			break;
		case 1: 
			if (segmentareS(0, 0, &faceROIS)) {
				rectangle(frame, faces[i], Scalar(0, 255, 0));
			}
			break;
		case 2:
			if (segmentareHS(0, 0, &faceROIS, &faceROIH)) {
				rectangle(frame, faces[i], Scalar(0, 255, 0));
			}
		}

	}
	imshow("Validated", frame);
	waitKey();
}

/*
	Validare fete pe imagini video
*/
void faceDetectDisplayVideo(int method) {
	int minFaceSize = 30;
	Mat prev, crnt, frame;
	Mat dst;
	Mat flow;

	VideoCapture cap(0);
	if (!cap.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey(0);
		return;
	}
	
	int frameNum = -1;
	char c;

	std::vector<Rect> faces;
	Mat frame_gray;
	Mat hsv;
	vector<Mat> channels;


	for (;;) {
		cap >> frame;
		if (frame.empty())
		{
			printf("End of video file\n");
			break;
		}
		++frameNum;

			if (frameNum > 0)
			{
				cvtColor(frame, frame_gray, CV_BGR2GRAY);
				cvtColor(frame, hsv, CV_BGR2HSV);
				equalizeHist(frame_gray, frame_gray);

				split(hsv, channels);

				face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(minFaceSize, minFaceSize));

				for (int i = 0; i < faces.size(); i++)
				{
					faces[i].x = faces[i].x + faces[i].width / 9;
					faces[i].y = faces[i].y + faces[i].width / 9;
					faces[i].width = faces[i].width * 7 / 9;
					faces[i].height = faces[i].height * 7 / 9;

					Mat faceROIH = channels[0](faces[i]) * 255 / 180;
					Mat faceROIS = channels[1](faces[i]);

					switch (method) {
					case 0:
						if (segmentareH(0, 0, &faceROIH)) {
							rectangle(frame, faces[i], Scalar(0, 255, 0));
						}
						break;
					case 1:
						if (segmentareS(0, 0, &faceROIS)) {
							rectangle(frame, faces[i], Scalar(0, 255, 0));
						}
						break;
					case 2:
						if (segmentareHS(0, 0, &faceROIS, &faceROIH)) {
							rectangle(frame, faces[i], Scalar(0, 255, 0));
						}
					}

				}
				imshow("Validated", frame);
				waitKey();
			}

		prev = crnt.clone();
		c = cvWaitKey(0); 
		if (c == 27) {
			printf("ESC pressed - playback finished\n\n");
			break;
		}
	}
}
/*
	Pornire algoritm pe imagini statice
*/
void faceDetectionImages() {
	int method = -1;
	printf("Optiuni:\n0 -- H\n1 -- S\n2 -- H + S\n");

	cin >> method;
	//scanf("Optiunea aleasa: %d ", &method);
	
	String face_cascade_name = "haarcascade_frontalface_alt.xml";

	// Load the cascades
	if (!face_cascade.load(face_cascade_name)) {
		printf("Error loading face cascades !\n");
		return;
	}
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat dst = src.clone();
		int minFaceSize = 30;

		faceDetectDisplay("Destination", dst, minFaceSize, method);
	}
}

/*
	Pornire algoritm de validare pe imagini live-video
*/
void faceDetectionVideo() {
	int method = -1;
	printf("Optiuni:\n0 -- H\n1 -- S\n2 -- H + S\n");

	cin >> method;
	//scanf("Optiunea aleasa: %d ", &method);

	String face_cascade_name = "haarcascade_frontalface_alt.xml";

	// Load the cascades
	if (!face_cascade.load(face_cascade_name)) {
		printf("Error loading face cascades !\n");
		return;
	}

	faceDetectDisplayVideo(method);
}

/*
	Detectie fete in poze
*/
void FaceDetectandDisplay(const string& window_name, Mat frame, int minFaceSize) {
	std::vector<Rect> faces;
	Mat frame_gray;
	Mat hsv;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	cvtColor(frame, hsv, CV_BGR2HSV);
	equalizeHist(frame_gray, frame_gray);

	vector<Mat> channels;
	split(hsv, channels);

	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(minFaceSize, minFaceSize));

	for (int i = 0; i < faces.size(); i++)
	{
		faces[i].x = faces[i].x + faces[i].width / 9;
		faces[i].y = faces[i].y + faces[i].width / 9;
		faces[i].width = faces[i].width * 7 / 9;
		faces[i].height = faces[i].height * 7 / 9;

		Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
		rectangle(frame, faces[i], Scalar(255, 0, 255));

		Mat faceROIH = channels[0](faces[i]) * 255 / 180;
		Mat faceROIS = channels[1](faces[i]);

		int localHistoH[256] = { 0 };
		int localHistoS[256] = { 0 };

		for (int j = 0; j < faceROIH.rows; j++) {
			for (int k = 0; k < faceROIH.cols; k++) {
				localHistoH[faceROIH.at<uchar>(j, k)]++;
				localHistoS[faceROIS.at<uchar>(j, k)]++;
			}
		}

		for (int s = 0; s < 256; s++) {
			printf("%d %d \n", localHistoH[s], localHistoS[s]);
		}

		for (int l = 0; l < 256; l++) {
			globalHistoH[l] += localHistoH[l];
			globalHistoS[l] += localHistoS[l];
		}
	}

	ftjGaussian();

	int sumaH = 0;
	int sumaS = 0;

	for (int i = 0; i < 256; i++) {
		sumaH += globalHistoHF[i];
		sumaS += globalHistoSF[i];
	}

	printf("\n\n");
	mediaH = 0;
	mediaS = 0;

	for (int i = 0; i < 256; i++) {
		mediaH += globalHistoHF[i] * i;
		mediaS += globalHistoSF[i] * i;

	}
	mediaH /= sumaH;
	mediaS /= sumaS;

	printf("MEDIA  %.2f %.2f\n", mediaH, mediaS);

	printf("\n\n");

	stdDevH = 0;
	stdDevS = 0;

	float sumaStdH = 0;
	float sumaStdS = 0;

	for (int i = 0; i < 256; i++) {
		sumaStdH += pow((i - mediaH), 2) * globalHistoHF[i];
		sumaStdS += pow((i - mediaS), 2) * globalHistoSF[i];
	}

	stdDevH = sqrt(sumaStdH / mediaH);
	stdDevS = sqrt(sumaStdS / mediaS);

	printf("Deviatia standard: H: %.2f ,S: %.2f \n", stdDevH, stdDevS);

	saveGlobalHistoH();
	saveGlobalHistoS();
}


/*
	Citire imagini multiple din folder pentru a antrena algoritmul pe ele.
*/
void openImagesFromFolder()
{
	String face_cascade_name = "haarcascade_frontalface_alt.xml";

	// Load the cascades
	if (!face_cascade.load(face_cascade_name)) {
		printf("Error loading face cascades !\n");
		return;
	}

	char folderName[MAX_PATH];
	if (openFolderDlg(folderName) == 0) {
		return;
	}

	char fname[MAX_PATH];
	FileGetter fg(folderName, "jpg");
	while (fg.getNextAbsFile(fname))
	{
		Mat src;
		src = imread(fname);
		Mat dst = src.clone();
		int minFaceSize = 30;
		FaceDetectandDisplay("Destination", dst, minFaceSize);

		if (waitKey() == 27) //ESC pressed
			break;
	}
}

/*
	Main function
*/

int main()
{
	int op;
	do
	{
		system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 1 - Validare fete pe imagini statice\n");
		printf(" 2 - Validare fete pe imagini live-video\n");

		printf(" 0 - Exit\n\n");
		scanf("%d",&op);
		switch (op)
		{
			case 1:
				faceDetectionImages();
				break;
			case 2: 
				faceDetectionVideo();
		}
	}
	while (op!=0);
	return 0;
}