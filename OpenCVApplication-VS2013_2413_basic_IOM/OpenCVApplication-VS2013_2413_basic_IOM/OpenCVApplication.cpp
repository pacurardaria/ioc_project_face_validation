// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include "Functions.h"
#include <queue>
#include <stdio.h>

//-------------Pentru fisiere: scriere, citire----------------
#include <iostream>
#include <fstream>
#include <conio.h>
//------------------------------------------------------------
using namespace std;

CascadeClassifier face_cascade;

int globalHistoH[256] = { 0 };
int globalHistoS[256] = { 0 };

float stdDevH = 7166.37;
float stdDevS = 4768.24;

float mediaH = 17.56;
float mediaS = 83.82;

int globalHistoHF[256] = { 0 };
int globalHistoSF[256] = { 0 };




bool isInside(int i, int j, Mat src) {

	if ((i >= 0 && i < src.cols) && (j >= 0 && j < src.rows)) {
		return true;
	}
	return false;
}

void testVideoSequence()
{
	VideoCapture cap("Videos/rubic.avi"); // off-line video from file
	//VideoCapture cap(0);	// live video from web cam
	if (!cap.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey(0);
		return;
	}
		
	Mat edges;
	Mat frame;
	char c;

	while (cap.read(frame))
	{
		Mat grayFrame;
		cvtColor(frame, grayFrame, CV_BGR2GRAY);
		Canny(grayFrame,edges,40,100,3);
		imshow("source", frame);
		imshow("gray", grayFrame);
		imshow("edges", edges);
		c = cvWaitKey(0);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished\n"); 
			break;  //ESC pressed
		};
	}
}

void MyCallBackFunc(int event, int x, int y, int flags, void* param)
{
	//More examples: http://opencvexamples.blogspot.com/2014/01/detect-mouse-clicks-and-moves-on-image.html
	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
		{
			printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
				x, y,
				(int)(*src).at<Vec3b>(y, x)[2],
				(int)(*src).at<Vec3b>(y, x)[1],
				(int)(*src).at<Vec3b>(y, x)[0]);
		}
}

void testMouseClick()
{
	Mat src;
	// Read image from file 
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		//Create a window
		namedWindow("My Window", 1);

		//set the callback function for any mouse event
		setMouseCallback("My Window", MyCallBackFunc, &src);

		//show the image
		imshow("My Window", src);

		// Wait until user press some key
		waitKey(0);
	}
}

/*void CallBackL4(int event, int x, int y, int flags, void *userdata)
{
	Mat* H = (Mat*)userdata;
	if (event == EVENT_RBUTTONDOWN)
	{
		int width = (*H).cols;
		int height = (*H).rows;
		Mat labels = Mat::zeros((*H).size(), CV_16UC1);
		Mat dst = Mat::zeros((*H).size(), CV_8UC1);
		queue<Point> que;
		double hue_avg = (*H).at<uchar>(y, x);
		int k = 1; 
		int N = 1; 
		que.push(Point(y, x)); 
		while (!que.empty()) { 
			Point oldest = que.front();
			que.pop(); 
			int xx = oldest.y; 
			int yy = oldest.x;

			for (int v = -1; v <= 1; v++)
				for (int u = -1; u <= 1; u++)
				{
					if (yy <= height - 1 && xx <= width - 1 && yy > 0 && xx > 0){
						if (abs((*H).at<uchar>(yy + v, xx + u) - hue_avg) < 20 && labels.at<ushort>(yy + v, xx + u) == 0){
							que.push(Point(yy + v, xx + u));
							labels.at<ushort>(yy + v, xx + u) = k;
							hue_avg = (N * hue_avg +(*H).at<uchar>(yy + v, xx + u)) / (N + 1);
							N++;
						}
					}
				}
		}

		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
			{
				if (labels.at<ushort>(i, j) != 0) {
					dst.at<uchar>(i, j) = 255;
				}
			}
		Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
		erode(dst, dst, element1, Point(-1, -1), 2);
		dilate(dst, dst, element1, Point(-1, -1), 4);
		erode(dst, dst, element1, Point(-1, -1), 2);
		imshow("dst", dst);

	}
}

void L4_RG()
{
	Mat src;
	Mat channels[3], hsv, H;

	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);

		GaussianBlur(src, src, Size(5, 5), 0, 0);
		cvtColor(src, hsv, CV_BGR2HSV);
		split(hsv, channels);
		H = channels[0] * 510 / 360;

		namedWindow("src", 1);
		setMouseCallback("src", CallBackL4, &H);

		imshow("src", src);
		waitKey(0);
	}
}*/

//void backgroundSubstraction() {
//
//	Mat frame, gray;
//	Mat backgnd;// background model
//	Mat diff;//differenceimage: |frame_gray -bacgnd| 
//	Mat dst;//output image/frame
//	char c;
//	int frameNum = -1;//current frame counter
//	const int method = 2;
//	// method = 
//	//			1-frame difference
//	//			2-running average
//	//			3-running average with selectivity
//	const unsigned char Th = 30;
//	const double alpha = 0.05;
//
//	VideoCapture cap("Videos/laboratory.avi"); // off-line video from file
//	//VideoCapture cap(0);	// live video from web cam
//	if (!cap.isOpened()) {
//		printf("Cannot open video capture device.\n");
//		waitKey(0);
//		return;
//	}
//
//	for (;;) {
//		cap >> frame; // achizitie frame nou
//		if (frame.empty())
//		{
//			printf("End of video file\n");
//			break;
//		}
//
//		++frameNum;
//		if (frameNum == 0)
//			imshow("sursa", frame);// daca este primul cadru se afiseaza doar sursa
//		cvtColor(frame, gray, CV_BGR2GRAY);
//		//Optional puteti aplica si unFTJ Gaussian
//
//		//Se initializeaza matricea / imaginea destinatie pentru fiecare frame
//		//dst = gray.clone(); // sau   
//		dst = Mat::zeros(gray.size(), gray.type());
//
//		const int channels_gray = gray.channels();
//		//restrictionam utilizarea metodei doarpt. imaginigrayscale cu un canal (8 bit/ pixel)
//		if (channels_gray > 1)
//			return;
//
//		if (frameNum > 0) // daca nu este primul cadru
//		{
//			absdiff(gray, backgnd, diff);
//			if (method == 1) {
//				backgnd = gray.clone();
//				for (int i = 0; i < diff.rows; i++) {
//					for (int j = 0; j < diff.cols; j++) {
//						if (diff.at<uchar>(i, j) > Th) {
//							dst.at<uchar>(i, j) = 255;
//						}
//					}
//				}
//			}
//			else if (method == 2) {
//				addWeighted(gray, alpha, backgnd, 1.0 - alpha, 0, backgnd);
//				for (int i = 0; i < diff.rows; i++) {
//					for (int j = 0; j < diff.cols; j++) {
//						if (diff.at<uchar>(i, j) > Th) {
//							dst.at<uchar>(i, j) = 255;
//						}
//					}
//				}
//			}
//			else if (method == 3) {
//				for (int i = 0; i < diff.rows; i++) {
//					for (int j = 0; j < diff.cols; j++) {
//						if (diff.at<uchar>(i, j) > Th) {
//							dst.at<uchar>(i, j) = 255;
//						}
//						else {
//							backgnd.at<uchar>(i, j) = alpha * gray.at<uchar>(i, j) + (1 - alpha)*backgnd.at<uchar>(i, j);
//							dst.at<uchar>(i, j) = 0;
//						}
//					}
//				}
//			}
//
//			Mat element1 = getStructuringElement(MORPH_CROSS, Size(3, 3));
//			erode(dst, dst, element1, Point(-1, -1), 2);
//			dilate(dst, dst, element1, Point(-1, -1), 2);
//
//			// Afiseaza imaginea sursa si destinatie
//			imshow("sursa", frame); // show source
//			imshow("dest", dst);// show destination
//			//Plasati aici codul pt. vizualizareaoricarorrezultate intermediare
//			// Ex: afisarea intr-o fereastra noua a imaginii diff
//			imshow("diff", diff);
//		}
//		else {
//
//			// daca este primul cadru, modelul de fundal este chiar el
//			backgnd = gray.clone();
//		}
//		// Conditia de avansare/terminare in cilului for(;;) de procesare
//		c = cvWaitKey(0);  // press any key to advance between frames
//		//for continous play use cvWaitKey( delay > 0)
//		if (c == 27) {
//			// press ESC to exit
//			printf("ESC pressed -playback finished\n");
//			break;  //ESC pressed
//		}
//
//	}
//}



//void FaceDetectandDisplay(const string& window_name, Mat frame, int minFaceSize)
//{
//	std::vector<Rect> faces;
//	equalizeHist(frame, frame);
//	//-- Detect faces
//	face_cascade.detectMultiScale(frame, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(minFaceSize, minFaceSize));
//	for (int i = 0; i < faces.size(); i++)
//	{
//		// get the center of the face
//		Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
//		// draw circle around the face
//		ellipse(frame, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
//		Mat faceROI = frame(faces[i]);
//	}
//	imshow(window_name, frame); //-- Show what you got
//	waitKey();
//}
//
//void faceDetectVideo(){
//	Mat prev, crnt, frame;
//	Mat dst;
//	Mat flow;
//	int minFaceSize = 30;
//	String face_cascade_name = "haarcascade_frontalface_alt.xml";
//
//	if (!face_cascade.load(face_cascade_name))
//	{
//		printf("Error loading face cascades !\n");
//		return;
//	}
//	//VideoCapture cap("Videos/walkcircle.avi"); // off-line video from file
//	VideoCapture cap(0);	// live video from web cam
//	if (!cap.isOpened()) {
//		printf("Cannot open video capture device.\n");
//		waitKey(0);
//		return;
//	}
//
//	int frameNum = -1;//crntent frame counter
//	char c;
//
//	for (;;)
//	{
//		cap >> frame; // get a new frame from camera
//		if (frame.empty())
//		{
//			printf("End of video file\n");
//			break;
//		}
//		++frameNum;
//		// functii de pre-procesare
//		cvtColor(frame, crnt, CV_BGR2GRAY);
//		GaussianBlur(crnt, crnt, Size(5, 5), 0.8, 0.8);
//			if (frameNum > 0) // not the first frame
//			{
//				FaceDetectandDisplay("Face detection", crnt, minFaceSize);
//			}
//		// store crntent frame as previos for the next cycle
//		prev = crnt.clone();
//		c = cvWaitKey(0); // press any key to advance between frames
//		//for continous play use cvWaitKey( delay > 0)
//		if (c == 27) {
//			// press ESC to exit
//			printf("ESC pressed - playback finished\n\n");
//			break; //ESC pressed
//		}
//	}
//}


void CallBackL4(int event, int x, int y, int flags, void *userdata) {

	Mat *H = (Mat*)userdata;
	if (event == EVENT_RBUTTONDOWN)
	{
		int width = (*H).cols;
		int height = (*H).rows;
		Mat labels = Mat::zeros((*H).size(), CV_16UC1);
		Mat dst = Mat::zeros((*H).size(), CV_8UC1);
		queue <Point> que;
		double hue_avg = (*H).at<uchar>(y, x);

		printf("%.2f \n", hue_avg);

		int k = 1;
		int N = 1;
		float T = 15;
		que.push(Point(x, y));

		int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
		int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

		while (!que.empty()) {
			Point oldest = que.front();
			que.pop();

			int xx = oldest.x;
			int yy = oldest.y;

			for (int i = 0; i < 8; i++) {
				if (isInside(xx + nX[i], yy + nY[i], *H)) {
					if ((abs((*H).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avg) < T)
						&& labels.at<ushort>(yy + nY[i], xx + nX[i]) == 0) {

						que.push(Point(xx + nX[i], yy + nY[i]));
						labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
						hue_avg = (N * hue_avg + (*H).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
						N++;

					}
				}
			}
		}


		printf("%d", N);

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (labels.at<ushort>(i, j) == 1) {
					dst.at<uchar>(i, j) = 255;
				}
			}
		}
		imshow("dst", dst);

		Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
		erode(dst, dst, element1, Point(-1, -1), 2);
		dilate(dst, dst, element1, Point(-1, -1), 4);
		erode(dst, dst, element1, Point(-1, -1), 2);
		imshow("clean dst", dst);
		waitKey();

	}
}

void L4_RG() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);
		Mat dstH = Mat(src.rows, src.cols, CV_8UC1);
		Mat dst = Mat(src.rows, src.cols, CV_8UC1);

		Mat hsvImg;
		Mat channels[3];
		GaussianBlur(src, src, Size(5, 5), 0, 0);
		cvtColor(src, hsvImg, CV_BGR2HSV);
		split(hsvImg, channels);
		dstH = channels[0] * 510 / 360;

		namedWindow("src", 1);
		setMouseCallback("src", CallBackL4, &dstH);
		imshow("src", src);
		waitKey();
	}
}


void saveGlobalHistoH(){
	FILE *fp;

	fp = fopen("HistogramaGlobalaH.txt", "w+");

	for (int count = 0; count < 256; count++){
		fprintf(fp, "%d\n", globalHistoHF[count]);
	}
	fprintf(fp, "%.2f\n", mediaH);
	fprintf(fp, "%.2f\n", stdDevH);
	fclose(fp);
}

void saveGlobalHistoS(){
	FILE *fp;

	fp = fopen("HistogramaGlobalaS.txt", "w+");

	for (int count = 0; count < 256; count++){
		fprintf(fp, "%d\n", globalHistoSF[count]);

	}
	fprintf(fp, "%.2f\n", mediaS);
	fprintf(fp, "%.2f\n", stdDevS);
	fclose(fp);

}

void readGlobalHistoH(){
	FILE *fp;
	int i = 0;
	int nr = -1;
	fp = fopen("HistogramaGlobalaH.txt", "r");
	for (int i = 0; i < 256; i++) {
		fscanf(fp, "%d\n", &nr);
		globalHistoH[i++] = nr;
	}
	fscanf(fp, "%.2f\n", &mediaH);
	fscanf(fp, "%.2f\n", &stdDevH);
	printf("%f\n %f\n", mediaH, stdDevH);

	fclose(fp);
}

void readGlobalHistoS(){
	FILE *fp;
	int i = 0;
	int nr = -1;
	fp = fopen("HistogramaGlobalaS.txt", "r");
	for (int i = 0; i < 256; i++) {
		fscanf(fp, "%d\n", &nr);
		globalHistoS[i++] = nr;
	}
	fscanf(fp, "%.2f\n", &mediaS);
	fscanf(fp, "%.2f\n", &stdDevS);
	printf("%f\n %f\n", mediaS, stdDevS);
	fclose(fp);
}

void readParameters(){
	printf("BEFORE");
	for (int i = 0; i < 256; i++) {
		printf("H: %d \n S: %d\n", globalHistoH[i], globalHistoS[i]);
	}
	printf("Media: %.2f Deviatia: %.2f\n", mediaH, stdDevH);

	readGlobalHistoH();
	readGlobalHistoS();
	
	printf("AFTER");
	for (int i = 0; i < 256; i++) {
		printf("H: %d \n S: %d\n", globalHistoH[i], globalHistoS[i]);
	}
	printf("Media: %.2f Deviatia: %.2f", mediaH, stdDevH);

}

void ftjGaussian() {
	float gauss[7];
	float sqrt2pi = sqrtf(2 * PI);
	float sigma = 1.5;
	float e = 2.718;
	float sum = 0;
	// Construire gaussian
	for (int i = 0; i<7; i++) {
		gauss[i] = 1.0 / (sqrt2pi*sigma)* powf(e, -(float)(i - 3)*(i - 3)
			/ (2 * sigma*sigma));
		sum += gauss[i];
	}
	// Filtrare cu gaussian
	for (int j = 3; j < 256 - 3; j++)
	{
		for (int i = 0; i < 7; i++) {
			globalHistoHF[j] += (float)globalHistoH[j + i - 3] * gauss[i];
			globalHistoSF[j] += (float)globalHistoS[j + i - 3] * gauss[i];
		}
	}
	//showHistogram("HF", globalHistoHF, 256, 256, true);
	//showHistogram("SF", globalHistoSF, 256, 256, true);

	int ThH = 350, ThS=250;
	for (int i = 0; i < 256; i++) {
		if (globalHistoHF[i] < ThH) {
			globalHistoHF[i] = 0;
		}
		if (globalHistoSF[i] < ThS) {
			globalHistoSF[i] = 0;
		}
	}
	//showHistogram("HF---", globalHistoHF, 256, 256, true);
	//showHistogram("SF---", globalHistoSF, 256, 256, true);
}

boolean segmentareH(int x, int y, void *userdata) {
	Mat *H = (Mat*)userdata;

	int width = (*H).cols;
	int height = (*H).rows;
	Mat labels = Mat::zeros((*H).size(), CV_16UC1);
	Mat dst = Mat::zeros((*H).size(), CV_8UC1);
	queue <Point> que;
	double hue_avg = mediaH;

	int k = 1;
	int N = 1;
	float T = 15;
	que.push(Point(x, y));

	int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
	int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

	while (!que.empty()) {
		Point oldest = que.front();
		que.pop();

		int xx = oldest.x;
		int yy = oldest.y;

		for (int i = 0; i < 8; i++) {
			if (isInside(xx + nX[i], yy + nY[i], *H)) {
				if ((abs((*H).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avg) < k * stdDevH)
					&& labels.at<ushort>(yy + nY[i], xx + nX[i]) == 0) {

					que.push(Point(xx + nX[i], yy + nY[i]));
					labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
					hue_avg = (N * hue_avg + (*H).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
					N++;
				}
			}
		}
	}

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (labels.at<ushort>(i, j) == 1) {
				dst.at<uchar>(i, j) = 255;
			}
		}
	}

	Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
	erode(dst, dst, element1, Point(-1, -1), 2);
	dilate(dst, dst, element1, Point(-1, -1), 4);
	erode(dst, dst, element1, Point(-1, -1), 2);
	//imshow("clean dst_H", dst);
	//waitKey();
	if (N / (width * height) > 0.6) {
		return true;
	}
	return false;
}

void faceDetectDisplay(const string& window_name, Mat frame, int minFaceSize, int method) {
	std::vector<Rect> faces;
	Mat frame_gray;
	Mat hsv;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	cvtColor(frame, hsv, CV_BGR2HSV);
	equalizeHist(frame_gray, frame_gray);

	vector<Mat> channels;
	split(hsv, channels);

	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(minFaceSize, minFaceSize));

	for (int i = 0; i < faces.size(); i++)
	{
		faces[i].x = faces[i].x + faces[i].width / 9;
		faces[i].y = faces[i].y + faces[i].width / 9;
		faces[i].width = faces[i].width * 7 / 9;
		faces[i].height = faces[i].height * 7 / 9;

		Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
		rectangle(frame, faces[i], Scalar(0, 0, 255));

		Mat faceROIH = channels[0](faces[i]) * 255 / 180;
		Mat faceROIS = channels[1](faces[i]);
		
		switch (method) {
		case 0:
			if (segmentareH(0, 0, &faceROIH)) {
				rectangle(frame, faces[i], Scalar(0, 255, 0));

			}
			break;
		}

	}
	imshow("frame", frame);
	waitKey();
}

void FaceDetectandDisplay(const string& window_name, Mat frame, int minFaceSize) {
	std::vector<Rect> faces;
	Mat frame_gray;
	Mat hsv;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	cvtColor(frame, hsv, CV_BGR2HSV);
	equalizeHist(frame_gray, frame_gray);

	vector<Mat> channels;
	split(hsv, channels);

	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(minFaceSize, minFaceSize));

	for (int i = 0; i < faces.size(); i++)
	{
		faces[i].x = faces[i].x + faces[i].width / 9;
		faces[i].y = faces[i].y + faces[i].width / 9;
		faces[i].width = faces[i].width * 7 / 9;
		faces[i].height = faces[i].height * 7 / 9;

		Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
		rectangle(frame, faces[i], Scalar(255, 0, 255));

		Mat faceROIH = channels[0](faces[i]) * 255 / 180;
		Mat faceROIS = channels[1](faces[i]);

		//imshow("roiH", faceROIH);
		//imshow("roiS", faceROIS);

		int localHistoH[256] = { 0 };
		int localHistoS[256] = { 0 };

		for (int j = 0; j < faceROIH.rows; j++) {
			for (int k = 0; k < faceROIH.cols; k++) {
				localHistoH[faceROIH.at<uchar>(j, k)]++;
				localHistoS[faceROIS.at<uchar>(j, k)]++;
			}
		}

		for (int s = 0; s < 256; s++) {
			printf("%d %d \n", localHistoH[s], localHistoS[s]);
		}

		for (int l = 0; l < 256; l++) {
			globalHistoH[l] += localHistoH[l];
			globalHistoS[l] += localHistoS[l];
		}
	}

	ftjGaussian();

	int sumaH = 0 ;
	int sumaS = 0;

	for (int i = 0; i < 256; i++) {
		sumaH += globalHistoHF[i];
		sumaS += globalHistoSF[i];
	}

	printf("\n\n");
	mediaH = 0;
	mediaS = 0;

	for (int i = 0; i < 256; i++) {
		mediaH += globalHistoHF[i] * i;
		mediaS += globalHistoSF[i] * i;

	}
	mediaH /= sumaH;
	mediaS /= sumaS;

	printf("MEDIA  %.2f %.2f\n", mediaH, mediaS);

	printf("\n\n");

	stdDevH = 0;
	stdDevS = 0;

	float sumaStdH = 0;
	float sumaStdS = 0;

	for (int i = 0; i < 256; i++) {
		sumaStdH += pow((i - mediaH) ,2) * globalHistoHF[i];
		sumaStdS += pow((i - mediaS), 2) * globalHistoSF[i];
	}

	stdDevH = sqrt(sumaStdH / mediaH);
	stdDevS = sqrt(sumaStdS / mediaS);

	printf("Deviatia standard: H: %.2f ,S: %.2f \n", stdDevH, stdDevS);

	saveGlobalHistoH();
	saveGlobalHistoS();
}

void faceDetection() {
	String face_cascade_name = "haarcascade_frontalface_alt.xml";

	// Load the cascades
	if (!face_cascade.load(face_cascade_name)) {
		printf("Error loading face cascades !\n");
		return;
	}
	Mat src;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		Mat dst = src.clone();
		int minFaceSize = 30;

		faceDetectDisplay("Destination", dst, minFaceSize, 0);
	}
}

void openImagesFromFolder()
{
	String face_cascade_name = "haarcascade_frontalface_alt.xml";

	// Load the cascades
	if (!face_cascade.load(face_cascade_name)) {
		printf("Error loading face cascades !\n");
		return;
	}

	char folderName[MAX_PATH];
	if (openFolderDlg(folderName) == 0) {
		return;
	}

	char fname[MAX_PATH];
	FileGetter fg(folderName, "jpg");
	while (fg.getNextAbsFile(fname))
	{
		Mat src;
		src = imread(fname);
		Mat dst = src.clone();
		int minFaceSize = 30;
		FaceDetectandDisplay("Destination", dst, minFaceSize);

		//imshow(fg.getFoundFileName(), src);

		if (waitKey() == 27) //ESC pressed
			break;
	}
	printf("ALOA LAO");
}


void incercare_callBackH(int event, int x, int y, int flags, void *userdata) {
	Mat *H = (Mat*)userdata;
	if (event == EVENT_RBUTTONDOWN)
	{
		int width = (*H).cols;
		int height = (*H).rows;
		Mat labels = Mat::zeros((*H).size(), CV_16UC1);
		Mat dst = Mat::zeros((*H).size(), CV_8UC1);
		queue <Point> que;
		double hue_avg = (*H).at<uchar>(y, x);

		int k = 1;
		int N = 1;
		float T = 15;
		que.push(Point(x, y));

		int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
		int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

		while (!que.empty()) {
			Point oldest = que.front();
			que.pop();

			int xx = oldest.x;
			int yy = oldest.y;

			for (int i = 0; i < 8; i++) {
				if (isInside(xx + nX[i], yy + nY[i], *H)) {
					if ((abs((*H).at<uchar>(yy + nY[i], xx + nX[i]) - hue_avg) < T)
						&& labels.at<ushort>(yy + nY[i], xx + nX[i]) == 0) {

						que.push(Point(xx + nX[i], yy + nY[i]));
						labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
						hue_avg = (N * hue_avg + (*H).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
						N++;
					}
				}
			}
		}

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (labels.at<ushort>(i, j) == 1) {
					dst.at<uchar>(i, j) = 255;
				}
			}
		}
		imshow("dst_H", dst);

		Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
		erode(dst, dst, element1, Point(-1, -1), 2);
		dilate(dst, dst, element1, Point(-1, -1), 4);
		erode(dst, dst, element1, Point(-1, -1), 2);
		imshow("clean dst_H", dst);
		waitKey();
	}
}

void incercare_callBackS(int event, int x, int y, int flags, void *userdata) {
	Mat *S = (Mat*)userdata;
	if (event == EVENT_RBUTTONDOWN)
	{
		int width = (*S).cols;
		int height = (*S).rows;
		Mat labels = Mat::zeros((*S).size(), CV_16UC1);
		Mat dst = Mat::zeros((*S).size(), CV_8UC1);
		queue <Point> que;
		double sat_avg = (*S).at<uchar>(y, x);

		int k = 1;
		int N = 1;
		float T = 15;
		que.push(Point(x, y));

		int nX[] = { -1, -1, -1, 0, 1, 1, 1, 0 };
		int nY[] = { -1, 0, 1, 1, 1, 0, -1, -1 };

		while (!que.empty()) {
			Point oldest = que.front();
			que.pop();

			int xx = oldest.x;
			int yy = oldest.y;

			for (int i = 0; i < 8; i++) {
				if (isInside(xx + nX[i], yy + nY[i], *S)) {
					if ((abs((*S).at<uchar>(yy + nY[i], xx + nX[i]) - sat_avg) < T)
						&& labels.at<ushort>(yy + nY[i], xx + nX[i]) == 0) {

						que.push(Point(xx + nX[i], yy + nY[i]));
						labels.at<ushort>(yy + nY[i], xx + nX[i]) = k;
						sat_avg = (N * sat_avg + (*S).at<uchar>(yy + nY[i], xx + nX[i])) / (N + 1);
						N++;
					}
				}
			}
		}

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (labels.at<ushort>(i, j) == 1) {
					dst.at<uchar>(i, j) = 255;
				}
			}
		}
		imshow("dst_S", dst);

		Mat element1 = getStructuringElement(MORPH_RECT, Size(3, 3));
		erode(dst, dst, element1, Point(-1, -1), 2);
		dilate(dst, dst, element1, Point(-1, -1), 4);
		erode(dst, dst, element1, Point(-1, -1), 2);
		imshow("clean dst_S", dst);
		waitKey();
	}
}

void incercare()
{
	Mat src;
	Mat hsv;
	Mat dst;
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		
		int height = src.rows;
		int width = src.cols;

		dst = Mat(height, width, CV_8UC1);

		//cream fereastra de afisare
		namedWindow("SourceH", 1);
		namedWindow("SourceS", 1);


		//componentele de culoare Hue si Sat a modelului HSV
		Mat H = Mat(height, width, CV_8UC1);
		Mat S = Mat(height, width, CV_8UC1);

		//definire vector pentru cele 3 canale ale hsv
		Mat channels[3];

		//Aplicam FTJ gaussian pt eliminarea zgomotelor
		GaussianBlur(src, src, Size(5, 5), 0, 0);

		//conversie RGB -> HSV
		cvtColor(src, hsv, CV_BGR2HSV);

		//salvam canalele hsv in vectorul channels
		split(hsv, channels);

		H = channels[0] * 510 / 360;
		//S = channels[1] * 510 / 360;

		setMouseCallback("SourceH", incercare_callBackH, &H);
		setMouseCallback("SourceS", incercare_callBackS, &S);
		imshow("SourceH", src);
		imshow("SourceS", src);
		waitKey();
	}
}


int main()
{
	//openImagesFromFolder();
	int op;
	do
	{
		system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 50 - Detect faces\n");
		//printf(" 51 - Read parameters: global histo, avg, std dev\n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");
		scanf("%d",&op);
		switch (op)
		{
			case 50:
				faceDetection();
				break;
		}
	}
	while (op!=0);
	return 0;
}